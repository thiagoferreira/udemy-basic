*** Settings ***
Library     RequestsLibrary
Library     Collections

*** Variable ***
${host}         https://fakerestapi.azurewebsites.net/
&{book}         ID=197
...             Title=Thiago Books
...             Description=book about robot
...             PageCount=10
...             Excerpt=None
...             PublishDate=2019-08-12T20:03:39.094Z

*** Keywords ***
Post a Book
    ${headers}      Create Dictionary         content-type=application/json
    Create Session      fake     ${host}
    ${response}   Post Request  fake    api/Books     data={"ID": 197,"Title": "Thiago Books","Description": "book about robot","PageCount": 10,"Excerpt": "None","PublishDate": "2019-08-12T20:03:39.094Z"}    headers=${headers}
     Log                 Resposta: ${response.json()}
    Should Be Equal As Strings    ${response.status_code}   200
    Dictionary Should Contain Item      ${response.json()}      ID               ${book.ID}
    Dictionary Should Contain Item      ${response.json()}      Title            ${book.Title}
    Dictionary Should Contain Item      ${response.json()}      Description      ${book.Description}
    Dictionary Should Contain Item      ${response.json()}      PageCount      ${book.PageCount}
    Dictionary Should Contain Item      ${response.json()}      Excerpt         ${book.Excerpt}
    Dictionary Should Contain Item      ${response.json()}      PublishDate      ${book.PublishDate}

Put a Book
    ${headers}      Create Dictionary         content-type=application/json
    ${responsePut}   Put Request  fake    api/Books/${book.ID}    data={"ID": 197,"Title": "Thiago Books","Description": "book about robot","PageCount": 10,"Excerpt": "None","PublishDate": "2019-08-12T20:03:39.094Z"}    headers=${headers}
     Log                 Resposta: ${responsePut.json()}
    Should Be Equal As Strings    ${responsePut.status_code}   200
    Dictionary Should Contain Item      ${responsePut.json()}      ID               ${book.ID}
    Dictionary Should Contain Item      ${responsePut.json()}      Title            ${book.Title}
    Dictionary Should Contain Item      ${responsePut.json()}      Description      ${book.Description}
    Dictionary Should Contain Item      ${responsePut.json()}      PageCount      ${book.PageCount}
    Dictionary Should Contain Item      ${responsePut.json()}      Excerpt         ${book.Excerpt}
    Dictionary Should Contain Item      ${responsePut.json()}      PublishDate      ${book.PublishDate}

Delete a Book
    ${headers}      Create Dictionary         content-type=application/json
    ${responseDelete}   Delete Request  fake    api/Books/${book.ID}   headers=${headers}
    Log                 Resposta: ${responseDelete.reason}
    Should Be Equal As Strings    ${responseDelete.status_code}   200
    Should Be Equal As Strings    ${responseDelete.reason}       OK


*** Test Cases ***
Make a simple REST API call
    Post a Book
    Put a Book
    Delete a Book