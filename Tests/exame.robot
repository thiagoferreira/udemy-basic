*** Settings ***
Library  SeleniumLibrary
Test Setup      Open test page
Test Teardown   Close Browser

*** Variables ***
${email}        id=email-input
${password}     id=password-input
${loginButton}  id=login-button
${msgSucess}    xpath=//div[@class='message success']
${msgFail}      xpath=//div[@class='message error']
${msgError}     css=[class='validation error']:first-child
${msgError2}    css=[class='validation error']:last-child

*** keywords ***
Open test page
    open browser                    https://codility-frontend-prod.s3.amazonaws.com/media/task_static/login_page/54431e1ace681edb2e3eb7478c542995/static/attachments/reference_page.html  chrome
    Wait Until Element Is Visible   ${email}
    Wait Until Element Is Visible   ${password}

Check if login work the valid credentials
    Input Text                      ${email}        login@codility.com
    Input Text                      ${password}     password
    Click Element                   ${loginButton}
    Element Text Should Be          ${msgSucess}     Welcome to Codility

Check if login return erro message with invalid credentials
    Input Text                      ${email}        unknow@codility.com
    Input Text                      ${password}     password
    Click Element                   ${loginButton}
    Element Text Should Be          ${msgFail}     You shall not pass! Arr!

Check if the email validation is working
    Input Text                      ${email}        unknow@codility
    Input Text                      ${password}     password
    Click Element                   ${loginButton}
    Element Text Should Be          ${msgError}     Enter a valid email

Check for empty credentials
    Click Element                   ${loginButton}
    Element Text Should Be          ${msgError}     Email is required
    Element Text Should Be          ${msgError2}    Password is required

*** Test Cases ***
Scenario 02
    Check if login work the valid credentials

Scenario 03
    Check if login return erro message with invalid credentials

Scenario 04
    Check if the email validation is working

Scenario 05
    Check for empty credentials