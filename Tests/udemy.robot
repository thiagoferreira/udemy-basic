*** Settings ***
Library  SeleniumLibrary
Resource  ../Resources/udemyKW.robot
Test Setup      Open selenium page
Test Teardown   Close Browser

*** Test Cases ***
Pesquisar Produtos Existentes
    Search for "Blouse" product
    Element Text Should Be          ${productLabel}     Blouse

Pesquisar Produtos não Existentes
    Search for produtoNãoExistente product
    Element Text Should Be          ${noResultsMsg}     No results were found for your search "produtoNãoExistente"

Listar Produtos
    [Tags]  debug
    Acess Summer Dresses category

Adicionar Produtos no Carrinho
    Search for t-shirt product
    Element Text Should Be          ${productLabel}     "T-SHIRT"
    Add to Cart

Remover Produtos
    Car Empty

Create Account
    Create Account

Log Random Email
    Log email   ${cliente.firstname}    ${cliente.lastname}     465466      ${cliente.email}