*** Settings ***
Resource  ../Resources/udemyObjects.robot

*** keywords ***
Open selenium page
    open browser                    http://automationpractice.com/index.php  firefox
    Wait Until Element Is Visible   ${searchBox}

Search for ${product} product
    Input Text                      ${searchBox}        ${product}
    Click Element                   ${searchButton}

Acess Summer Dresses category
    Mouse Over                      ${womenCategory}
    Wait Until Element Is Visible   ${smDressesCategory}
    Click Element                   ${smDressesCategory}
    Element Text Should Be          ${categoryLabel}     SUMMER DRESSES 

Add to Cart
    Click Element                   ${addToCartButton}
    Wait Until Element Is Visible   ${ChekoutButton}
    Click Element                   ${ChekoutButton}
    Element Text Should Be          ${cartsummary}     Your shopping cart

Car Empty
    Click Element                   ${cartButon}
    Element Text Should Be          ${noResultsMsg}     Your shopping cart is empty.

Create Account
     Click Element                   ${siginButton}
    Input Text                      ${emailField}        ${cliente.email}
    Click Element                   ${createAccButton}
    Wait Until Element Is Visible   ${country}
    Input Text                      id=customer_firstname    ${cliente.firstname}
    Input Text                      id=customer_lastname        ${cliente.lastname}
    Input Text                      id=firstname        ${cliente2[0]}
    Input Text                      id=lastname        ${cliente2[1]}
    Select From List By Index       ${country}        1
    Sleep   1s
    Select From List By Label       ${stateField}      Arizona

Log email
    [Arguments]     ${name}     ${lastname}    ${string}    ${email}
    Log     ${name}${lastname}${string}${email}