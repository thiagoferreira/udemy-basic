*** Variables ***
${searchBox}       id=search_query_top
${searchButton}    css=.button-search
${productLabel}    css=.page-heading span
${noResultsMsg}    css=p.alert-warning
${womenCategory}   xpath=//a[@class='sf-with-ul' and @title='Women']
${smDressesCategory}    css=[title="Summer Dresses"]:first-child
${categoryLabel}   css=span.cat-name
${addToCartButton}  css=[title="Add to cart"] span
${cartsummary}     css=span.navigation_page
${ChekoutButton}   css=[title="Proceed to checkout"]
${cartButon}       css=[title="View my shopping cart"]
${siginButton}     css=.login
${emailField}      id=email_create
${createAccButton}  id=SubmitCreate
${stateField}      xpath=//*[@id="id_state"]
${country}      id=id_country
&{cliente}     firstname=Thiago    lastname=Ferreira   email=test@test.com
@{cliente2}     Rua Nove de Abril   951 3 ESQ